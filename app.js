const express = require('express');
var fs = require('fs')
var https = require('https')
const app = express();

const dotenv = require('dotenv');
dotenv.config();

app.use(express.static('public'));

const { auth, requiresAuth } = require('express-openid-connect');
const port = process.env.PORT || 3000;


app.use(express.static('public'));
app.set('view engine', 'pug')

const config = {
    authRequired: false,
    auth0Logout: true,
    issuerBaseURL: process.env.ISSUER_BASE_URL,
    baseURL: process.env.BASE_URL,
    clientID: process.env.CLIENT_ID,
    secret: process.env.SECRET,
    idpLogout: true, //login not only from the app, but also from identity provider
    clientSecret: process.env.CLIENT_SECRET,
    authorizationParams: {
        response_type: 'code',
        //scope: "openid profile email"   
    },
};
// auth router attaches /login, /logout, and /callback routes to the baseURL
app.use(auth(config));

app.get('/', function(req, res) {
    req.user = {
        isAuthenticated: req.oidc.isAuthenticated()
    };
    if (req.user.isAuthenticated) {
        req.user.name = req.oidc.user.name;
        req.user.last_login = req.oidc.user.last_login;
        req.user.updated_at = req.oidc.user.updated_at;
        res.render('signedHome', {
            user: req.user,
            readFile: fs.readFileSync
        });
    } else {
        res.render('home', { user: req.user, lana: 'Lana' });
    }
});

app.get('/private', requiresAuth(), function(req, res) {
    const user = JSON.stringify(req.oidc.user);
    res.render('private', { user });
});

app.get("/sign-up", (req, res) => {
    res.oidc.login({
        returnTo: '/',
        authorizationParams: {
            screen_hint: "signup",
        },
    });
});



app.listen(port, function() {
    console.log(`Server running at https://localhost:${port}/`);
});

/*
https.createServer({
        key: fs.readFileSync('server.key'),
        cert: fs.readFileSync('server.cert')
    }, app)
    .listen(port, function() {
        console.log(`Server running at https://localhost:${port}/`);
    });
*/




/*
const express = require('express');
var https = require('https')
const app = express();
require('dotenv').config();
const { auth, requiresAuth } = require('express-openid-connect');

app.use(
    auth({
        authRequired: false,
        auth0Logout: true,
        issuerBaseURL: process.env.ISSUER_BASE_URL,
        baseURL: process.env.BASE_URL,
        clientID: process.env.CLIENT_ID,
        secret: process.env.SECRET,
    })
)


app.get('/', (req, res) => {
    res.sendFile(req.oidc.isAuthenticated() ? 'view/signedHome.html' : 'view/home.html', { root: __dirname, user: req.oidc.user });
});


app.get1('/', function(req, res) {
    req.user = {
        isAuthenticated: req.oidc.isAuthenticated()
    };
    if (req.user.isAuthenticated) {
        req.user.name = req.oidc.user.name;
        res.render('signedHome', { user: req.user });
    } else {
        res.render('home');
    }
});


app.get('/profile', requiresAuth(), (req, res) => {
    res.send(JSON.stringify(req.oidc.user));
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`listening on port ${port}`);
});
*/